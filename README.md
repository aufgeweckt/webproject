# Getslash web project startpoint

## Author
Michel Frömmge | michel.froemmgen@getslash.de

Use this bootstrap 4.0 based theme as an foundation and starting point of your website development. Customize it for your needs.

## Requirements
* git > https://git-scm.com/
* node.js > https://nodejs.org/en/
* gulp cli > https://gulpjs.com/


## Installation Guide
```
git clone ggit@gitlab.com:aufgeweckt/webproject.git
```
```
npm install
```

## Develpoment / Start gulp
```
gulp serve
```

Gulp pipes and distribute on demand into the theme folder (sass, js-min). A build task is not needed.

## Dependencies

Install new dependencies always with ```--save``` flag. (e.g. ```npm i --save gulp-sass```)

You may need to install the following dependencies by manually.
### gulp-sass
``` 
npm i gulp-sass
```
### gulp-minify
```
npm i gulp-minif
```
### gulp-include
```
npm i gulp-include
```

## Bower components and other scripts

Add bower components or any other scripts by 
```
//=require <path>
```
Please note: you do not have to point on 'bower_conponents' or 'scripts' directory, both a are defined in 'gulpfile.js' as root. 