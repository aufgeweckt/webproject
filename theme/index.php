<!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Getslash</title>
		<link rel="stylesheet" href="assets/css/style.css">

		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125014006-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-125014006-1');
		</script>
		
	</head>
	<body>
		<div class="container">

			<div class="content">

				<img class="branding" src="assets/images/gs-branding.png" alt="Getslash GmbH" title="Getslash GmbH">

		

				<?php
					$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
						switch ($lang){
					    case "de":
					        include("de.php");
					        break;
					    case "en":
					        include("en.php");
					        break;        
					    default:
					        include("en.php");
					        break;
						}
					?>



			<ul class="social-media">
				<li><a href="https://de.linkedin.com/company/getslash-gmbh"><img src="assets/images/linkedin.svg" alt="linkedIn"></a></li>	
				<li><a href="https://www.xing.com/jobs/oelde-abteilungsleiter-it-operations-41623899?sc_o=da980_e"><img src="assets/images/xing.svg" alt="Xing"></a></li>
			</ul>

		</div>
	</div>
						<div class="compass"><img src="assets/images/compass.png" alt=""></div>	
		<script src="assets/js-min/scripts-min.js"></script>
	</body>
</html>