// get dependencies
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var minify = require('gulp-minify');
var include = require('gulp-include');

// Static Server + watching scss/html files
gulp.task('serve-bs', ['styles'], function() {

    browserSync.init({
        server: "./theme"
    });

    gulp.watch("theme/assets/css/scss/*.scss", ['styles']);
    gulp.watch('theme/assets/js-min/*.js').on('change', browserSync.reload);
    gulp.watch("theme/*.html").on('change', browserSync.reload);
});


// Compile SCSS to CSS
gulp.task('styles', function () {
  return gulp.src('./theme/assets/css/scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./theme/assets/css/'))
    .pipe(browserSync.stream());
});

// Include scripts in just one file and minify
gulp.task("scripts", function() { 
  return gulp.src("./theme/assets/scripts/scripts.js")
    .pipe(include({
       hardFail: true,
       includePaths: [
        __dirname + "/bower_components",
        __dirname + "/theme/assets/scripts/"
      ]
    }))

    .pipe(minify({
      ext:{
        //src:'-debug.js',
        min:'-min.js'
      },
      exclude: ['tasks'],
      ignoreFiles: ['.combo.js', '-min.js', '.min.js']
    }))
    .on('error', console.log)

  .pipe(gulp.dest("./theme/assets/js-min"))
  .pipe(browserSync.stream());
});

// watching for changes
gulp.task('watch', function () {
  gulp.watch('./theme/assets/css/scss/*.scss', ['styles'] );
  gulp.watch('./theme/assets/scripts/*.js', ['scripts'] );
});

// define terminal commands
gulp.task('serve', ['watch']);
gulp.task('serve-sync', ['watch', 'serve-bs']);
gulp.task('default', ['styles', 'scripts']);